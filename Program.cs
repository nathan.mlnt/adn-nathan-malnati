﻿/**
 * Projet : ADN
 * Auteur : Nathan Malnati
 * Détail : Calcul du nombre d'occurence des bases ACTG
 * Date : 14.10.2022
 * Version : 1.0
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("C:\\Users\\nathan.mlnt\\Desktop\\2022-2023\\vendredi\\ACTG\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();
            int compterA = 0;
            int compterG = 0;
            int compterC = 0;
            int compterT = 0;
            while (line != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    

                    if (line[0] == 'A')
                    {
                        compterA++;
                    }

                    if (line[0] == 'G')
                    {
                        compterG++;
                    }
                    if (line[0] == 'C')
                    {
                        compterC++;
                    }
                    if (line[0] == 'T')
                    {
                        compterT++;
                    }             
                }
                line = s.ReadLine();

            }
             Console.WriteLine("compter A = " + compterA);
             Console.WriteLine("compter C = " + compterC);
             Console.WriteLine("compter T = " + compterT);
             Console.WriteLine("compter G = " + compterG);           
             Console.ReadKey();
               



            }
        }
    }

